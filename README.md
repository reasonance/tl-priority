# [Priority] Tech Task (Titanium Labs)


This is a tech task for QAA position in Titanium Labs.


* Install Cypress
* How to run tests in this repository
* Steps that work on my machine

### Install Cypress

This point is pefectly expanded in the [Cypress Documentation](https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements).


## How to run tests in this repository

There are several ways to make tests executed: from command line or from default Cypress Console.
In both cases it is required to pass environment variables to the test.

**Pass env. variables through cypress.env.json file**

In the first case you can create **cypress.env.json** in the root project's folder with the following content:

```
{
    "login": "XXX",
    "password": "XXX",
    "company": "XXX"
}
```

The 'XXX' values should be exchanged with the real data.

**Pass env. variables through command line options**

`cypress run --spec "cypress/integration/tests/*" --env login=XXX,password=XXX,company=XXX`

The 'XXX' values should be exchanged with the real data.

## Steps that work on my machine

1) Install Cypress

`npm install cypress --save-dev`

2) Install Cypress XPath

`npm install -D cypress-xpath`

3) Run tests

`$(npm bin)/cypress run --spec "cypress/integration/tests/*" --env login="XXX",password="XXX",company="XXX"`