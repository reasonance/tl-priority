import LoginPage from "../pages/login_page";
import HomePage from "../pages/home_page";

context('Login', function() {
    it('Login the application', function() {
        const loginPage = new LoginPage();
        loginPage.open();
        loginPage.login(Cypress.env('login'), Cypress.env('password'));

        const homePage = new HomePage();
        homePage.isOnPage();
        homePage.isSelectCompanyPopupDisplayed();
    })
})