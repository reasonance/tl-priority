import HomePage from "../pages/home_page";

describe('Add to home page', function() {
    this.beforeEach('Login the app and select company', () => {
        cy.uiLogin(Cypress.env('login'), Cypress.env('password'));
        cy.uiSelectCompany(Cypress.env('company'));
        // api call to remove the 'Income Pie' from subjects
        // api call to remove the 'Advanced Finance Main' from subjects
    })
    it('Add portlet to Homepage', function() {
        const homePage = new HomePage();
        const menuItemName = 'Income Pie';
        homePage.addPortletToHomePage(menuItemName);
        homePage.isMenuItemPresentOnPage(menuItemName);
    })
    it('Add Tile to Homepage', function() {
        const homePage = new HomePage();
        const searchQuery = 'main wizard';
        const expectedEntityNameInPopup = 'Advanced Finance Main Wizard';
        const expectedEntityNameOnHomePage = 'Advanced Finance Main';
        homePage.addTileToHomePage(searchQuery, expectedEntityNameInPopup);
        homePage.isMenuItemPresentOnPage(expectedEntityNameOnHomePage);
    })
})