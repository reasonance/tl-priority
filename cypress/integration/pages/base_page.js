import TopHeader from '../elements/top_header';
import BottomHeader from '../elements/bottom_header';

export default class BasePage {

    constructor() {
        this.topHeader = new TopHeader();
        this.bottomHeader = new BottomHeader();
    }
    
    open(url) {
        cy.visit(url, {timeout: 15000});
    }

    isTopHeaderDisplayed() {
        this.topHeader.elements.root().should('be.visible');
    }

    isBottomHeaderDisplayed() {
        this.bottomHeader.elements.root().should('be.visible');
    }

    /**
     * 
     * @param {string} companyName 
     */
    isCompanySelected(companyName) {
        this.bottomHeader.elements.selectedCompanyName()
        .should('be.visible')
        .and('contain.text', companyName)
    }
}