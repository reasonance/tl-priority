import BasePage from './base_page'

export default class LoginPage extends BasePage {
  constructor() {
    super();
    this.elements = {
      loginForm: () => cy.get('.loginlabel_panelStyle', {timeout: 15000}),
      loginField: () => cy.get('input[type=\'text\'][class=\'loginlabel_textboxstyle\']'),
      passwordField: () => cy.get('input[type=\'password\'][class=\'loginlabel_textboxstyle\']'),
      okButton: () => cy.xpath('//button[text()=\'OK\' and @class=\'loginlabel_buttonStyle1\']')
    };
  }

  open() {
    super.open('/');
    this.elements.loginForm().should('be.visible');
  }

/**
 * 
 * @param {string} login 
 * @param {string} password 
 */
login(login, password) {
  this.fillInLoginFiled(login);
  this.fillInPasswordFiled(password);
  this.clickLoginButton();
}

  /**
   * @param {string} login
   */
  fillInLoginFiled(login) {
    this.elements.loginField().type(login)
    .should('have.value', login);
  }

  /**
   * @param {string} password
   */
  fillInPasswordFiled(password) {
    this.elements.passwordField().type(password);
  }

  clickLoginButton() {
    this.elements.okButton()
    .click();
  }
}
