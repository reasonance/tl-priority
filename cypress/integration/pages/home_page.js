import BasePage from "./base_page";
import TopSearchSection from "../elements/sections/top_search_section";
import SubjectsSection from "../elements/sections/subjects_section";
import SelectCompanyPopup from "../elements/popups/select_company_popup";
import AddPortletToHomePagePopup from "../elements/popups/add_portlet_to_home_page_popup";
import AddSubjectToHomePagePopup from "../elements/popups/add_subject_to_home_page_popup";

export default class HomePage extends BasePage {
    constructor() {
        super();
        this.topSearchSection = new TopSearchSection();
        this.selectCompanyPopup = new SelectCompanyPopup();
        this.addPortletToHomePagePopup = new AddPortletToHomePagePopup();
        this.addSubjectToHomePagePopup = new AddSubjectToHomePagePopup();
        this.subjectsSection = new SubjectsSection();
    }
  
    open() {
      super.open('/');
      this.isOnPage();
    }

    isOnPage() {
        super.isTopHeaderDisplayed();
        super.isBottomHeaderDisplayed();
    }

    isSelectCompanyPopupDisplayed() {
        this.selectCompanyPopup.isDisplayed();
    }

    /**
     * 
     * @param {string} companyName 
     */
    selectCompany(companyName) {
        this.selectCompanyPopup.clickShowCompaniesListButton();
        this.selectCompanyPopup.selectCompanyFromList(companyName);
        this.selectCompanyPopup.isCompanyNameInputFieldContains(companyName);
        this.selectCompanyPopup.clickSelectCompanyOKButton();
        super.isCompanySelected(companyName);
    }

    /**
     * 
     * @param {string} option 
     */
    addPortletToHomePage(option) {
        this.topSearchSection.clickAddPortletToHomePageButton();
        this.addPortletToHomePagePopup.isDisplayed();
        this.addPortletToHomePagePopup.selectOption(option);
        this.addPortletToHomePagePopup.clickAddButton();
        this.addPortletToHomePagePopup.closePopup();
    }

    /**
     * 
     * @param {string} option 
     */
    addTileToHomePage(searchQuery, expectedEntityName) {
        this.topSearchSection.clickAddTileToHomepageButton();
        this.addSubjectToHomePagePopup.isDisplayed();
        this.addSubjectToHomePagePopup.checkIgnoreCaseCheckbox();
        this.addSubjectToHomePagePopup.fillInSearchEntityField(searchQuery);
        this.addSubjectToHomePagePopup.ckickSearch();
        this.addSubjectToHomePagePopup.clickOnFoundEntity(expectedEntityName);
        this.addSubjectToHomePagePopup.clickAddButton();
        this.addSubjectToHomePagePopup.closePopup();
    }

    isMenuItemPresentOnPage(menuItemName) {
        this.subjectsSection.isItemPresentInMenuWithName(menuItemName);
    }
  }