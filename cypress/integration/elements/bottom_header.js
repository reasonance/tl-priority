export default class BottomHeader {

    constructor() {
        this.elements = {
            root: () => cy.get('.bottomHeader'),
            search: () => cy.get('#searchbottomHeader'),
            tabs: () => cy.get('.bottomHeader ul li'),
            selectedCompanyName: () => cy.get('#companyName')
        }
    }
}