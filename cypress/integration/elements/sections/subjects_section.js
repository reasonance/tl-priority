export default class SubjectsSection {

    constructor() {
        this.elements = {
            root: () => cy.get('.subjectsContainer'),
            menuItems: () => cy.get('.subjectsMenuItem')
        }
    }

    isItemPresentInMenuWithName(menuItemName) {
        this.elements.menuItems().contains(menuItemName).should('be.visible');
    }

}