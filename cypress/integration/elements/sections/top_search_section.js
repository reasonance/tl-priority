export default class TopSearchSection {

    constructor() {
        this.elements = {
            addPortletToHomePageButton: () => cy.get('.add-subject-item-portlet'),
            addTileToHomePageButton: () => cy.get('.add-subject-item')
        }
    }

    clickAddPortletToHomePageButton() {
        this.elements.addPortletToHomePageButton().click();
    }

    clickAddTileToHomepageButton() {
        this.elements.addTileToHomePageButton().click();
    }
}