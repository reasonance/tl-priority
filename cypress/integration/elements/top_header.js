export default class TopHeader {

    constructor() {
        this.elements = {
            root: () => cy.get('.topHeader'),
        }
    }
}