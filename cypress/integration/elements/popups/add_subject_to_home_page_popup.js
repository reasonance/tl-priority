export default class AddSubjectToHomePagePopup {

    constructor() {
        this.elements = {
            root: () => cy.get('#PriModalDialog'),
            ignoreCaseCheckBox: () => cy.get('.FindEntDlgIgnoreCase input'),
            searchEntityField: () => cy.get('.searchEntityTop .TextBox_SearchFor'),
            searchEntityButton: () => cy.get('.searchEntityTop .GrnBut_SearchFor'),
            foundEntities: () => cy.get('.findEntTitle'),
            addButton: () => cy.get('.html-face'),
            xIcon:() => cy.get('.xbutton-up')
        }
    }

    isDisplayed() {
        this.elements.root().should('contain.text', 'Add to Home Page');
    }

    checkIgnoreCaseCheckbox() {
        this.elements.ignoreCaseCheckBox().check();
    }

    /**
     * 
     * @param {string} searchQuery 
     */
    fillInSearchEntityField(searchQuery) {
        this.elements.searchEntityField().type(searchQuery);
    }

    ckickSearch() {
        this.elements.searchEntityButton().click();
    }

    /**
     * 
     * @param {string} expectedEntityName 
     */
    clickOnFoundEntity(expectedEntityName) {
        this.elements.foundEntities().contains(expectedEntityName).click();
    }

    clickAddButton() {
        this.elements.addButton().contains('Add').click();
    }

    closePopup() {
        this.elements.xIcon().click();
    }
}