export default class AddPortletToHomePagePopup {

    constructor() {
        this.elements = {
            root: () => cy.get('#PriModalDialog'),
            options: () => cy.get('.findEntTitle'),
            addButton: () => cy.get('.html-face'),
            xIcon: () => cy.get('.xbutton-up')
        }
    }

    isDisplayed() {
        this.elements.root().should('contain.text', 'Add to Home Page');
    }

    selectOption(option) {
        this.elements.options().contains(option).click();
    }

    clickAddButton() {
        this.elements.addButton().contains('Add').click();
    }

    closePopup() {
        this.elements.xIcon().click();
        this.elements.root().should('not.be.visible');
    }
}