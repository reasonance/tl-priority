export default class SelectCompanyPopup {

    constructor() {
        this.elements = {
            root: () => cy.get('#PriModalDialog', {timeout: 15000}),
            showCompaniesListButton: () => cy.get('.selectCompanyDlgContent g'),
            companiesTable: () => cy.get('table[name=\'SearchResultsTable\'] div'),
            companyNameInputField: () => cy.get('.choosecompanydlg_textBoxStyle', {timeout: 10000}),
            selectCompanyOKButton: () => cy.get('.html-face'),
        }
    }

    isDisplayed() {
        this.elements.root()
        .should('contain.text', 'Select Company');
    }

    clickShowCompaniesListButton() {
        this.elements.showCompaniesListButton()
        .should('be.visible')
        .click();
    }

    /**
     * 
     * @param {string} companyName 
     */
    selectCompanyFromList(companyName) {
        this.elements.companiesTable().contains(companyName).click();
    }

    /**
     * 
     * @param {string} companyName 
     */
    isCompanyNameInputFieldContains(companyName) {
        this.elements.companyNameInputField()
        .should('contain.text', companyName)
    }

    clickSelectCompanyOKButton() {
        this.elements.selectCompanyOKButton().contains('OK').click();
    }
}