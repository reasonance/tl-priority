import LoginPage from '../integration/pages/login_page';
import HomePage from '../integration/pages/home_page';

Cypress.Commands.add("defaultLogin", (login, password) => {
    cy.readFile('cypress/fixtures/loginRequestBody.xml')
    .then(txtBody => {

        var xmlBody = new DOMParser().parseFromString(txtBody, 'text/xml');
        var userNamePrefix = xmlBody.getElementsByTagName('Username')[0].textContent;
        var userName = xmlBody.getElementsByTagName('Username')[0].textContent = login + userNamePrefix;
        var passwordType = xmlBody.getElementsByTagName('Password')[0].textContent = password;
        var pass = xmlBody.getElementsByTagName('password')[0].textContent = password;
        txtBody = new XMLSerializer().serializeToString(xmlBody);
        
        cy.request({
            url: 'https://us.priority-software.com/demus/priwcf/service.svc',
            method: 'POST',
            body: txtBody,
            headers: {  
                "authority" : "us.priority-software.com",
                "soapaction" : "http://tempuri.org/IWCFService/GeneralValidPassword",
                "content-type" : "text/xml; charset=UTF-8"
            }
        })
    })
})

Cypress.Commands.add("uiLogin", (login, password) => {
    const loginPage = new LoginPage();
    loginPage.open();
    loginPage.login(login, password);

    const homePage = new HomePage();
    homePage.isOnPage();
    homePage.isSelectCompanyPopupDisplayed();
})

Cypress.Commands.add("uiSelectCompany", (company) => {
    const homePage = new HomePage();
    homePage.selectCompany(company);
})
